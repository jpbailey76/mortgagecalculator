import PackageDescription

let package = Package(
    name: "server",
    dependencies: [
        .Package(url: "https://jpbailey76@bitbucket.org/jpbailey76/week6local.git", majorVersion: 3),
        .Package(url: "https://jpbailey76@bitbucket.org/jpbailey76/week6.git", majorVersion: 3),
        .Package(url: "https://github.com/IBM-Swift/Kitura.git", majorVersion: 1),
        .Package(url: "https://github.com/IBM-Swift/HeliumLogger.git", majorVersion: 1),
        .Package(url: "https://github.com/IBM-Swift/Kitura-StencilTemplateEngine.git", majorVersion: 1)
    ]
)
