import Kitura
import HeliumLogger
import LoggerAPI
import KituraStencil
import Foundation
@testable import MorgageCalculator
@testable import FutureValue


let logger = HeliumLogger()
logger.colored = true
Log.logger = logger

let router = Router()
router.setDefault(templateEngine: StencilTemplateEngine())

router.get("/") {
    request, response, next in
    try response.render("home", context: [:])
    // Check for query
    if request.queryParameters["balance"] == nil {
    	print("No passed values yet")
    } else {
    	next()
    }
}

// Create loan class
var loan = Loan(balance: 0.0, length: 0, interest: 0)

router.get("/") { 
	request, response, next in

	let balanceReq = request.queryParameters["balance"] ?? ""
	let lengthReq = request.queryParameters["length"] ?? ""
	let interestReq = request.queryParameters["interest"] ?? ""

	if let b = Double(balanceReq), let l = Int(lengthReq), let i = Double(interestReq){
		loan = Loan(balance: Double(balanceReq)!, length: Int(lengthReq)!, interest: Double(interestReq)!)
		loan.monthlyPayment = loan.calculate()
	}
	else{
		
	}

	next()
}

router.get("/") {
    request, response, next in

    try response.redirect("/calculations")
}

router.get("/calculations") {
	request, response, next in

	// Context to contain data
	var futureVal:Double = 0.0
	futureVal = futureValue(monthlyPayment: loan.monthlyPayment, rate: loan.interest, length: loan.length)
	var data:[String:Any] = ["balance": round(100*loan.balance)/100, "length": loan.length, "interest": loan.interest,
							"monthlyPayment": round(100*loan.monthlyPayment)/100,
							"futureValue": round(100*futureVal)/100]

	loan = Loan(balance: 0.0, length: 0, interest: 0)

	try response.render("calculations", context: data)
}

router.get("/formulas") {
	request, response, next in
	try response.render("formulas", context: [:])

}

Kitura.addHTTPServer(onPort: 8090, with: router)
Kitura.run()
