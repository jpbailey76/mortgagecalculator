# README #

### What is this repository for? ###

* This is a server-side Swift application which uses the Kitura web-framework to host on your local network

### How do I get set up? ###
* Setup & Configuration
1. Clone the repo
2. Build using: swift build
3. run using ./.build/debug/server
4. To access the app visit: localhost:8090/ in your browser of choice

### Dependencies ###
* Dependencies are located in Package.swift